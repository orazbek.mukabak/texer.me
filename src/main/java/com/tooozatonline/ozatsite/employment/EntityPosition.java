package com.tooozatonline.ozatsite.employment;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "position")
public class EntityPosition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Size(max = 127, message = "The title must have maximum 127 characters.")
    @Column(length = 127)
    private String title;
    @Size(max = 255, message = "The description must have maximum 255 characters.")
    @Column(length = 255)
    private String description;
    @Size(min = 1, max = 3, message = "The priority must be between 0 to 999 numbers.")
    @Column(nullable = false, length = 3)
    private int priority;
    @Column(name = "createddate")
    private LocalDateTime createdDate;
    @Size(min = 1, max = 1, message = "The status must be 1 character.")
    @Column(name = "status", nullable = false, length = 1)
    private char status;
}