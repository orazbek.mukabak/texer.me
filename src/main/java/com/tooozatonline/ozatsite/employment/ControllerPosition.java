package com.tooozatonline.ozatsite.employment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/position")
public class ControllerPosition {
    @Autowired
    private ServicePosition servicePosition;

    @RequestMapping("/read-position/{id}")
    protected ModelAndView readPositionJSP(@PathVariable int id, Model model) {
        EntityPosition position = servicePosition.findById(id);
        model.addAttribute("position", position);
        return new ModelAndView("employment/read-position");
    }

    @RequestMapping("/edit")
    protected ModelAndView edit() {
        return new ModelAndView("employment/edit-position");
    }

    @PostMapping("/create")
    protected EntityPosition create(@RequestBody EntityPosition position) {
        return servicePosition.create(position);
    }

    @GetMapping("/read/{id}")
    protected EntityPosition read(@PathVariable int id) {
        return servicePosition.read(id);
    }

    @GetMapping("/read-all")
    protected List<EntityPosition> readAll() {
        return servicePosition.readAll();
    }

    @PostMapping("/update")
    protected EntityPosition update(@RequestBody EntityPosition position) {
        return servicePosition.update(position);
    }

    @DeleteMapping(value = "/delete/{id}")
    protected ResponseEntity<Integer> delete(@PathVariable int id) {
        servicePosition.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}