package com.tooozatonline.ozatsite.userinfo;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class ControllerInit {
    @RequestMapping("/")
    protected ModelAndView index() {
        return new ModelAndView("public/index");
    }

//    @RequestMapping("/signin")
//    private ModelAndView signin() {
//        return new ModelAndView("public/signin");
//    }

    @RequestMapping("/testing1")
    protected ModelAndView testing1() {
        return new ModelAndView("public/testing1");
    }
}