package com.tooozatonline.ozatsite.userinfo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RepositoryUserInfo extends JpaRepository<EntityUserInfo, Integer> {
    Optional<EntityUserInfo> findByMobile(String username);
}