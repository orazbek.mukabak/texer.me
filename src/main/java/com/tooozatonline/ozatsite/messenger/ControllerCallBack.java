package com.tooozatonline.ozatsite.messenger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/callback")
public class ControllerCallBack {
    @Autowired
    private ServiceCallBack serviceMessenger;
    @PostMapping(path = "/create")
    private EntityCallBack createCallBack(@RequestBody EntityCallBack callback) {
        return serviceMessenger.create(callback, callback.getMessage());
    }
}