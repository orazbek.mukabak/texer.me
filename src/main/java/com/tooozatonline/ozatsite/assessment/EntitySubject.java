package com.tooozatonline.ozatsite.assessment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.Gson;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@JsonIgnoreProperties(value = {"titles"})
@Table(name = "subject")
public class EntitySubject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Size(min = 1, max = 127, message = "The title must have at least 1 and maximum 127 characters.")
    @Column(name = "name", nullable = false, length = 127)
    private String name;
//    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "fk_subject_id", referencedColumnName = "id")
//    private Set<EntityTitle> titles = new HashSet<>();
}