package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ServiceQuestion {
    @Autowired
    private RepositoryQuestion repositoryQuestion;

    protected EntityQuestionMCQ createMCQ(EntityQuestionMCQ mcq, int titleId) {
        mcq.setTitleId(titleId);
        return repositoryQuestion.save(mcq);
    }

    //protected Optional<EntityQuestion> read(int id) {
    //      return repositoryQuestion.findById(id);
    //}

    protected List<EntityQuestion> readAll() {
        return repositoryQuestion.findAll();
    }

    protected List<EntityQuestion> readAllByTitleId(int titleId) {
        return repositoryQuestion.readAllByTitleId(titleId);
    }

    protected List<EntityQuestion> readAllByIdList(Collection<Integer> idList) {
        return repositoryQuestion.readAllByIdIn(idList);
    }
}