package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/subject")
public class ControllerSubject {
    @Autowired
    private ServiceSubject serviceSubject;

    @RequestMapping("/edit")
    protected ModelAndView edit() {
        return new ModelAndView("assessment/edit-subject");
    }

    @PostMapping("/create")
    protected EntitySubject create(@RequestBody EntitySubject subject) {
        return serviceSubject.create(subject);
    }

    @GetMapping("/read/{id}")
    protected Optional<EntitySubject> read(@PathVariable int id) {
        return serviceSubject.read(id);
    }

    @GetMapping("/read-all")
    protected List<EntitySubject> readAll() {
        return serviceSubject.readAll();
    }

    @PostMapping("/update")
    protected EntitySubject update(@RequestBody EntitySubject subject) {
        return serviceSubject.update(subject);
    }

    @DeleteMapping(value = "/delete/{id}")
    protected ResponseEntity<Integer> delete(@PathVariable int id) {
        serviceSubject.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}