package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceSubject {
    @Autowired
    private RepositorySubject repositorySubject;

    protected EntitySubject create(EntitySubject subject) {
        return repositorySubject.save(subject);
    }

    protected Optional<EntitySubject> read(int id) {
        return repositorySubject.findById(id);
    }

    protected List<EntitySubject> readAll() {
        return repositorySubject.findAll();
    }
    protected EntitySubject update(EntitySubject subject) {
        EntitySubject temp = repositorySubject.findEntitySubjectById(subject.getId());
        temp.setName(subject.getName());
        return repositorySubject.save(subject);
    }
    protected void delete(int id) {
        repositorySubject.deleteById(id);
    }
}