package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceVariant {
    @Autowired
    private RepositoryVariant repositoryVariant;

    protected EntityVariant create(EntityVariant variant, int fieldId) {
        variant.setFieldId(fieldId);
        return repositoryVariant.save(variant);
    }

    protected Optional<EntityVariant> read(int id) {
        return repositoryVariant.findById(id);
    }

    protected List<EntityVariant> readAllByFieldId(int id) {
        return repositoryVariant.findEntityVariantByFieldId(id);
    }

    protected EntityVariant update(EntityVariant variant) {
        EntityVariant temp = repositoryVariant.findEntityVariantById(variant.getId());
        temp.setName(variant.getName());
        return repositoryVariant.save(temp);
    }

    protected void delete(int id) {
        repositoryVariant.deleteById(id);
    }
}