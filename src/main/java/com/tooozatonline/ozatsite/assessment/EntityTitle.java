package com.tooozatonline.ozatsite.assessment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "title")
//@JsonIgnoreProperties(value = {"questions"})
public class EntityTitle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Size(min = 1, max = 127, message = "The title must have at least 1 and maximum 127 characters.")
    @Column(name = "name", nullable = false, length = 127)
    private String name;
    @Column(name = "fk_subject_id", nullable = false)
    private int subjectId;

//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "fk_title_id", referencedColumnName = "id")
//    private Set<EntityQuestion> questions = new HashSet<>();
}