package com.tooozatonline.ozatsite.assessment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepositoryTitle extends JpaRepository<EntityTitle, Integer> {

    EntityTitle findEntityTitleById(int id);

    List<EntityTitle> findEntityTitleBySubjectId(int id);
}