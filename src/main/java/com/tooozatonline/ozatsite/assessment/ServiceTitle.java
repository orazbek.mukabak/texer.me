package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceTitle {
    @Autowired
    private RepositoryTitle repositoryTitle;

    protected EntityTitle create(EntityTitle title, int subjectId) {
        title.setSubjectId(subjectId);
        return repositoryTitle.save(title);
    }

    protected Optional<EntityTitle> read(int id) {
        return repositoryTitle.findById(id);
    }

    protected List<EntityTitle> readAllBySubjectId(int id) {
        return repositoryTitle.findEntityTitleBySubjectId(id);
    }

    protected EntityTitle update(EntityTitle title) {
        EntityTitle temp = repositoryTitle.findEntityTitleById(title.getId());
        temp.setName(title.getName());
        return repositoryTitle.save(temp);
    }

    protected void delete(int id) {
        repositoryTitle.deleteById(id);
    }
}