package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/task")
public class ControllerTask {
    @Autowired
    private ServiceTask serviceTask;

    @PostMapping("/create")
    protected EntityTask create(@RequestBody EntityTask task, @RequestParam int variantId) {
        return serviceTask.create(task, variantId);
    }

    @GetMapping("/read/{id}")
    protected Optional<EntityTask> read(@PathVariable int id) {
        return serviceTask.read(id);
    }

    @GetMapping("/read-all-by-variantid/{id}")
    protected List<EntityTask> readAllByVariantId(@PathVariable int id) {
        return serviceTask.readAllByVariantId(id);
    }
}