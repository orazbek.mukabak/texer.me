package com.tooozatonline.ozatsite.assessment;

import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDateTime;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
abstract class EntityQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "IdGenerator")
    @TableGenerator(table = "questionsequence", name = "IdGenerator")
    @Column(name = "id")
    private int id;
    @Column(name = "fk_title_id", nullable = false)
    private int titleId;
}