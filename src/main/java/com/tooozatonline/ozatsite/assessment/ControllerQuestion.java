package com.tooozatonline.ozatsite.assessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/question")
public class ControllerQuestion {
    @Autowired
    private ServiceQuestion serviceQuestion;

    @PostMapping("/create-mcq")
    protected EntityQuestion createMCQ(@RequestBody EntityQuestionMCQ mcq, @RequestParam int titleId) {
        return serviceQuestion.createMCQ(mcq, titleId);
    }

    @GetMapping("/read-all")
    protected List<EntityQuestion> readAll() {
        return serviceQuestion.readAll();
    }

    @GetMapping("/read-all-by-titleid/{id}")
    protected List<EntityQuestion> readAllByTitleId(@PathVariable int id) {
        return serviceQuestion.readAllByTitleId(id);
    }

    @GetMapping("/read-all-by-idlist/{idList}")
    protected List<EntityQuestion> readAllByIdList(@PathVariable Collection<Integer> idList) {
        System.out.println(serviceQuestion.readAllByIdList(idList).toString());
        return serviceQuestion.readAllByIdList(idList);
    }
}