package com.tooozatonline.ozatsite.assessment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.management.loading.MLetContent;
import java.util.Collection;
import java.util.List;

public interface RepositoryQuestion extends JpaRepository<EntityQuestion, Integer> {

    List<EntityQuestion> readAllByIdIn(Collection<Integer> id);

    List<EntityQuestion> readAllByTitleId(int titleId);
}