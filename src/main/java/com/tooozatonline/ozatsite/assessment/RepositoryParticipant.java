package com.tooozatonline.ozatsite.assessment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface RepositoryParticipant extends JpaRepository<EntityParticipant, Integer> {
    EntityParticipant findEntityParticipantById(int id);

    EntityParticipant findEntityParticipantByVariantIdAndAuthor(int VariantId, int author);
}