package com.tooozatonline.ozatsite.assessment;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@Builder
@Table(name = "participant")
public class EntityParticipant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "author")
    private int author;
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;
    @Column(name = "fk_variant_id", nullable = false)
    private int variantId;
    @Column(name = "totalquestion")
    private int totalQuestion;
    @Column(name = "totalcorrectquestion")
    private double totalCorrectQuestion;
    @Column(name = "correctanswers", length = 4095)
    private String correctAnswers;
    @Column(name = "answers", length = 4095)
    private String answers;
    @Column(name = "questionids", length = 1023)
    private String questionIds;
    @Column(name = "taskids", length = 1023)
    private String taskIds;
    @Column(name = "createddate")
    private LocalDateTime createdDate;
    @Column(name = "status", nullable = false, length = 1)
    private char status;

    public EntityParticipant() {
    }

    public EntityParticipant(int author, String firstName, String lastName, int variantId, int totalQuestion, String correctAnswers, String questionIds, String taskIds, LocalDateTime createdDate, char status) {
        this.author = author;
        this.firstName = firstName;
        this.lastName = lastName;
        this.variantId = variantId;
        this.totalQuestion = totalQuestion;
        this.correctAnswers = correctAnswers;
        this.questionIds = questionIds;
        this.taskIds = taskIds;
        this.createdDate = createdDate;
        this.status = status;
    }

    public EntityParticipant(int id, int author, String firstName, String lastName, int variantId, int totalQuestion, double totalCorrectQuestion, String correctAnswers, String answers, String questionIds, String taskIds, LocalDateTime createdDate, char status) {
        this.id = id;
        this.author = author;
        this.firstName = firstName;
        this.lastName = lastName;
        this.variantId = variantId;
        this.totalQuestion = totalQuestion;
        this.totalCorrectQuestion = totalCorrectQuestion;
        this.correctAnswers = correctAnswers;
        this.answers = answers;
        this.questionIds = questionIds;
        this.taskIds = taskIds;
        this.createdDate = createdDate;
        this.status = status;
    }
}