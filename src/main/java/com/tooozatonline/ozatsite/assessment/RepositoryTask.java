package com.tooozatonline.ozatsite.assessment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepositoryTask extends JpaRepository<EntityTask, Integer> {

    List<EntityTask> findByVariantIdOrderByPriorityAsc(int variantId);
}