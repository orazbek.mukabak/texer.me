package com.tooozatonline.ozatsite.assessment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepositoryVariant extends JpaRepository<EntityVariant, Integer> {

    EntityVariant findEntityVariantById(int id);

    List<EntityVariant> findEntityVariantByFieldId(int id);
}