package com.tooozatonline.ozatsite.website;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RepositoryBanner extends JpaRepository<EntityBanner, Integer> {
    @Query(value = "SELECT * FROM banner b WHERE b.url = ?1", nativeQuery = true)
    List<EntityBanner> readAllByUrl(String url);
}