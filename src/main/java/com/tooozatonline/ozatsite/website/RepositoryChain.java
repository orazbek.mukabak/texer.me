package com.tooozatonline.ozatsite.website;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryChain extends JpaRepository<EntityChain, String> {
}