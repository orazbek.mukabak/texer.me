package com.tooozatonline.ozatsite.website;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ServiceChain {
    @Autowired
    private RepositoryChain repositoryChain;
    protected EntityChain create(EntityChain Chain) {
        return repositoryChain.save(Chain);
    }
    protected Optional<EntityChain> read(String url) {
        if(url.contains("www.")) {
            url = url.substring(4);
        }
        return repositoryChain.findById(url);
    }
    protected List<EntityChain> readAll() {
        return repositoryChain.findAll();
    }
}