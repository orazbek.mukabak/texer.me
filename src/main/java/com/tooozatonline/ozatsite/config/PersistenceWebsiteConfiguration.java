package com.tooozatonline.ozatsite.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({"classpath:website-persistence-mysql.properties"})
@EnableJpaRepositories(
        basePackages = "com.tooozatonline.ozatsite.website",
        entityManagerFactoryRef = "websiteEntityManager",
        transactionManagerRef = "websiteTransactionManager"
)
public class PersistenceWebsiteConfiguration {
    @Autowired
    private Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean websiteEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(websiteDataSource());
        em.setPackagesToScan(new String[]{"com.tooozatonline.ozatsite.website"});
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("website.hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("website.hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("website.hibernate.show_sql"));
        properties.put("hibernate.enable_lazy_load_no_trans", env.getProperty("website.hibernate.enable_lazy_load_no_trans"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public DataSource websiteDataSource() {
        DriverManagerDataSource websiteDataSource = new DriverManagerDataSource();
        websiteDataSource.setDriverClassName(env.getProperty("website.jdbc.driver"));
        websiteDataSource.setUrl(env.getProperty("website.jdbc.url"));
        websiteDataSource.setUsername(env.getProperty("website.jdbc.user"));
        websiteDataSource.setPassword(env.getProperty("website.jdbc.password"));
        return websiteDataSource;
    }

    @Bean
    public PlatformTransactionManager websiteTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(websiteEntityManager().getObject());
        return transactionManager;
    }
}