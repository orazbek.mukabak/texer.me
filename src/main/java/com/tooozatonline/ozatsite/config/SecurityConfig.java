package com.tooozatonline.ozatsite.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {
    @Bean
    public UserDetailsService userDetailsService() {
        return new UserInfoUserDetailsService();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http.csrf().disable()
                .authorizeHttpRequests()
                .requestMatchers(
                        "/view/public/**", // Access to the public folder
                        "/view/assessment/**",
                        "/view/employment/**",
                        "/css/**", // Access to the public folder
                        "/images/**", // Access to the public folder
                        "/js/**", // Access to the public folder
                        "/", // Controller index.jsp opener
                        //        "/signin", // Controller index.jsp opener

                        "/callback/create", // Controller access, so anyone can request for callback
                        "/banner/read-all-by-url/**", // Controller access, so anyone can see banners by url
                        "/chain/read/**", // Controller access, so anyone can read chain
                        "/question/**",
                        "/question/read-all-mcq-by-titleid/**",
                        "/subject/**",
                        "/subject/create",
                        "/subject/update",
                        "/title/read-all-by-subjectid/**",
                        "/title/**",
                        "/title/create",
                        "/title/update",
                        "/participant/**",
                        "/position/**",
                        "/position/read-position",
                        "/position/read-position/**",
                        "/variant/**",
                        "/task/**",
                        "/userinfo/create").permitAll()
                .and()
                .authorizeHttpRequests().requestMatchers("/course/**")
                .authenticated().and().formLogin()
                .loginPage("/?#popupSignin")
                .loginProcessingUrl("/authenticate")
                .failureUrl("/")
                .defaultSuccessUrl("/")
                .permitAll().and().build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService());
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }
}