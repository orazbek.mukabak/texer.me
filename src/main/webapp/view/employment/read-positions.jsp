<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/employment/read-positions.css"/>
    <title></title>
</head>
<body>
<div id="wrapper_read_positions">
</div>
<script>
    function drawPositionReadPositions(position) {
        let varHtml = "<div class='card sticky-action'>";
        varHtml += "<div class='card-image waves-effect waves-block waves-light'>";
        //varHtml += "<img class='activator' src='${pageContext.request.contextPath}/images/employment/position.jpg' />";
        varHtml += "</div>";
        varHtml += "<div class='card-content'>";
        varHtml += "<span class='card-title activator grey-text text-darken-4'>" + position.title + "<i class='material-icons-outlined right'>more_vert</i></span>";
        varHtml += "<p><a class='btn-open-position-read-positions' href='${pageContext.request.contextPath}/position/read-position/" + position.id + "'>Емтихан тапсыру</a></p>";
        varHtml += "</div>";
        varHtml += "<div class='card-reveal'>";
        varHtml += "<span class='card-title grey-text text-darken-4'>" + position.title + "<i class='material-icons-outlined right'>close</i></span>";
        varHtml += "<p>" + position.description + "</p>";
        varHtml += "</div>";
        varHtml += "</div>";
        return varHtml;
    }

    function drawPositionsReadPositions(positions) {
        let varHtml = "<div class='cards-wrapper-read-positions'>";
        for (let i = 0; i < positions.length; i++) {
            varHtml += drawPositionReadPositions(positions[i]);
        }
        varHtml += "</div>";
        document.getElementById("wrapper_read_positions").innerHTML += varHtml;
    }

    function readAllReadPositions() {
        fetch('${pageContext.request.contextPath}/position/read-all')
            .then(response => response.json())
            .then(data => {
                if (data.length > 0) {
                    drawPositionsReadPositions(data);
                }
            });
    }

    readAllReadPositions();
</script>
</body>
</html>