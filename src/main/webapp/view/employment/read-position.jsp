<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title></title>
</head>
<body>
<%@ include file="../public/header.jsp" %>
<div>
    <div>
    ${position.title} мамандығы бойынша емтихан тапсыру
    <br>Жалпы ережелер:
        <br>1.
        <br>2.
        <br>3.
        <br>3.${position.title} мамандығы бойынша емтихан тапсыру тілін талдаңыз
    </div>
    <div>
        <div onclick="readTasks(1, '2023-12-12 12:34:29.000000');">
            Қазақша
        </div>
        <div onclick="readTasks(1, '2023-12-12 11:34:29.000000');">
            Орысша
        </div>
    </div>
</div>

<%@ include file="../assessment/read-tasks.jsp" %>
<%@ include file="../public/footer.jsp" %>
</body>
</html>