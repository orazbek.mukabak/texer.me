<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-fields.css"/>
</head>
<body>
<div class="read-fields-wrapper">
    <div class="left-wrapper-read-fields">
        <div class="collapsible-wrapper">
            <ul class="collapsible" id="field_list"></ul>
        </div>
        <div class='new-field'>
            <input id="new_field_name" type="text" placeholder="Жаңа пән атауы" minlength="1" maxlength="127">
            <button onclick="newFieldReadFields();"><span class="material-icons-outlined">add</span></button>
        </div>
    </div>
    <div class="right-wrapper-read-fields" id="right_wrapper_read_fields">
        <div class="placeholder-header-wrapper-read-fields">
            ${exam.title}
        </div>
        <div class="placeholder-wrapper-read-fields">
            ${exam.description}
        </div>
    </div>
</div>
<script>
    function updateFieldReadFields(id, oldName) {
        Swal.fire({
            title: 'Пәннің атауын өңдеу',
            input: 'text',
            inputValue: oldName,
            inputAttributes: {autocapitalize: 'off'},
            showCancelButton: true,
            confirmButtonColor: '#222c31',
            cancelButtonColor: '#222c31',
            confirmButtonText: 'Иә',
            cancelButtonText: 'Жоқ',
            preConfirm: (name) => {

                fetch('${pageContext.request.contextPath}/field/update', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({"id": id, "name": name})
                })
                    .then(response => {
                        M.toast({
                            html: '<span><spring:message code="signup.successful.message" /></span>',
                            displayLength: 2000,
                            classes: 'green',
                            completeCallback: () => {
                                window.location.href = "${pageContext.request.contextPath}/exam/read-exam/${exam.id}";
                            }
                        });
                    })

            }

        });
    }

    function deleteFieldReadFields(id, name) {
        Swal.fire({
            title: '"' + name + '" пәндік атауын өшіресіз бе?',
            text: "Осы пәндік атау ішіндегі барлық тақырыптар мен құрастырылған тесттер түбегейлі жойылатын болады!",
            showCancelButton: true,
            confirmButtonColor: '#222c31',
            cancelButtonColor: '#222c31',
            confirmButtonText: 'Иә',
            cancelButtonText: 'Жоқ'
        }).then((result) => {
            if (result.value) {
                fetch('${pageContext.request.contextPath}/field/delete/' + id, {
                    method: 'DELETE',
                })
                    .then(res => res.text())
                    .then(res => {
                            M.toast({
                                html: '<span><spring:message code="signup.successful.message" /></span>',
                                displayLength: 2000,
                                classes: 'green',
                                completeCallback: () => {
                                    window.location.href = "${pageContext.request.contextPath}/exam/read-exam/${exam.id}";
                                }
                            });
                        }
                    )
            }
        });
    }

    function postFieldReadFields(field) {
        fetch('${pageContext.request.contextPath}/field/create?examId=${exam.id}', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(field)
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    if (response.id && response.id > 0) {
                        M.toast({
                            html: '<span><spring:message code="signup.successful.message" /></span>',
                            displayLength: 2000,
                            classes: 'green',
                            completeCallback: () => {
                                window.location.href = "${pageContext.request.contextPath}/exam/read-exam/${exam.id}";
                            }
                        });
                    } else {
                        M.toast({
                            html: '<span><spring:message code="signup.tech.support.message" /></span>',
                            classes: 'red accent-2'
                        });
                    }
                } else {
                    M.toast({
                        html: '<span><spring:message code="signup.tech.support.message" /></span>',
                        classes: 'red accent-2'
                    });
                }
            })
    }

    function newFieldReadFields() {
        let name = document.getElementById("new_field_name").value;
        if (name.length > 0 && name.length < 127) {
            postFieldReadFields({"name": name});
        } else {
            M.toast({
                html: '<span><spring:message code="signup.invalid.phone.message" /></span>',
                classes: 'red accent-2'
            });
        }
    }

    function drawFieldReadFields(field) {
        let varHtml = "";
        varHtml += "<li>";
        varHtml += "<div class='wrapper-collapsible-header'><div class='collapsible-header' onclick='readVariants(" + field.id + ");'>" + field.name + "</div>";
        varHtml += "<div class='field-drop'>";
        varHtml += "<a class='dropdown-trigger btn' href='#' data-target='dropdown_field_" + field.id + "'><span class='material-icons-outlined'>arrow_drop_down</span></a>";
        varHtml += "<ul id='dropdown_field_" + field.id + "' class='dropdown-content'>";
        varHtml += "<li><a href='javascript:updateFieldReadFields(" + field.id + ",\"" + field.name + "\");'><span class='material-icons-outlined'>drive_file_rename_outline</span></a></li>";
        varHtml += "<li><a href='javascript:deleteFieldReadFields(" + field.id + ",\"" + field.name + "\");'><span class='material-icons-outlined'>delete</span></a></li>";
        varHtml += "</ul>";
        varHtml += "</div>";
        varHtml += "</div><div id='field_" + field.id + "_wrapper' class='collapsible-body'></div>";
        varHtml += "</li>";
        return varHtml;
    }

    function drawFieldsReadFields(fields) {
        let varHtml = "";
        for (let i = 0; i < fields.length; i++) {
            varHtml += drawFieldReadFields(fields[i]);
        }
        document.getElementById("field_list").innerHTML += varHtml;
        $(document).ready(function () {
            $('.collapsible').collapsible();
        });

        $('.dropdown-trigger').dropdown();
    }

    function readFields() {
        fetch('${pageContext.request.contextPath}/field/read-all-by-examid/${position.id}')
            .then(response => response.json())
            .then(data => {
                console.log(data);
                if (data.length > 0) {
                    drawFieldsReadFields(data);
                }
            });
    }

    readFields();
</script>
</body>
</html>