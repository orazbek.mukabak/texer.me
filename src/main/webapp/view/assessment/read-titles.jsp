<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-titles.css"/>
</head>
<body>
<script>
    function newTitleReadSubjects(id) {
        let name = document.getElementById("new_title_name_" + id).value;
        if (name.length > 0 && name.length < 127) {
            postTitleReadSubjects({"name": name, "subjectId": id});
        } else {
            M.toast({
                html: '<span><spring:message code="signup.invalid.phone.message" /></span>',
                classes: 'red accent-2'
            });
        }
    }

    function updateTitleReadSubjects(id, oldName, subjectId) {
        Swal.fire({
            title: 'Атауын өңдеу',
            input: 'text',
            inputValue: oldName,
            inputAttributes: {autocapitalize: 'off'},
            showCancelButton: true,
            confirmButtonColor: '#222c31',
            cancelButtonColor: '#222c31',
            confirmButtonText: 'Иә',
            cancelButtonText: 'Жоқ',
            preConfirm: (name) => {

                fetch('${pageContext.request.contextPath}/title/update', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({"id": id, "name": name, "subjectId": subjectId})
                })
                    .then(response => {
                        M.toast({
                            html: '<span><spring:message code="signup.successful.message" /></span>',
                            displayLength: 2000,
                            classes: 'green',
                            completeCallback: () => {
                                readTitles(subjectId);
                            }
                        });
                    })

            }

        });
    }

    function deleteTitleReadSubjects(id, name, subjectId) {
        Swal.fire({
            title: '"' + name + '" атауын өшіресіз бе?',
            text: "Осы атау ішіндегі барлық сұрақтар түбегейлі жойылатын болады!",
            showCancelButton: true,
            confirmButtonColor: '#222c31',
            cancelButtonColor: '#222c31',
            confirmButtonText: 'Иә',
            cancelButtonText: 'Жоқ'
        }).then((result) => {
            if (result.value) {
                fetch('${pageContext.request.contextPath}/title/delete/' + id, {
                    method: 'DELETE',
                })
                    .then(res => res.text())
                    .then(res => {
                            M.toast({
                                html: '<span><spring:message code="signup.successful.message" /></span>',
                                displayLength: 2000,
                                classes: 'green',
                                completeCallback: () => {
                                    readTitles(subjectId);
                                }
                            });
                        }
                    )
            }
        });
    }

    function postTitleReadSubjects(title) {
        fetch('${pageContext.request.contextPath}/title/create?subjectId=' + title.subjectId, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(title)
        })
            .then(response => response.json())
            .then(response => {
                if (response) {
                    if (response.id && response.id > 0) {
                        M.toast({
                            html: '<span><spring:message code="signup.successful.message" /></span>',
                            displayLength: 2000,
                            classes: 'green',
                            completeCallback: () => {
                                readTitles(response.subjectId);
                            }
                        });
                    } else {
                        M.toast({
                            html: '<span><spring:message code="signup.tech.support.message" /></span>',
                            classes: 'red accent-2'
                        });
                    }
                } else {
                    M.toast({
                        html: '<span><spring:message code="signup.tech.support.message" /></span>',
                        classes: 'red accent-2'
                    });
                }
            })
    }

    function drawTitleReadSubjects(title) {
        let varHtml = "";
        varHtml += "<li class='collection-item'>";
        varHtml += "<div class='title-text-wrapper-read-subjects' onclick='readQuestions(" + title.id + ");'>";
        varHtml += "<div class='title-text-read-subjects'>" + title.name + "</div>";
        varHtml += "</div>";
        varHtml += "<div class='subject-drop'>";
        varHtml += "<a class='dropdown-trigger btn' href='#' data-target='dropdown_title_" + title.id + "'><span class='material-icons-outlined'>arrow_drop_down</span></a>";
        varHtml += "<ul id='dropdown_title_" + title.id + "' class='dropdown-content'>";
        varHtml += "<li><a href='javascript:updateTitleReadSubjects(" + title.id + ",\"" + title.name + "\", " + title.subjectId + ");'><span class='material-icons-outlined'>drive_file_rename_outline</span></a></li>";
        varHtml += "<li><a href='javascript:deleteTitleReadSubjects(" + title.id + ",\"" + title.name + "\", " + title.subjectId + ");'><span class='material-icons-outlined'>delete</span></a></li>";
        varHtml += "</ul>";
        varHtml += "</div>";
        varHtml += "</li>";
        return varHtml;
    }

    function drawTitlesReadSubjects(titles, subjectId) {
        let varHtml = "<div class='collection'>";
        for (let i = 0; i < titles.length; i++) {
            varHtml += drawTitleReadSubjects(titles[i]);
        }
        varHtml += "</div>";
        varHtml += "<div class='new-title'>";
        varHtml += "<input id='new_title_name_" + subjectId + "' type='text' placeholder='Жаңа тақырып атауы'>";
        varHtml += "<button onclick='newTitleReadSubjects(" + subjectId + ");'><span class='material-icons-outlined'>add</span></button>";
        varHtml += "</div>";
        document.getElementById("subject_" + subjectId + "_wrapper").innerHTML = varHtml;
        $('.dropdown-trigger').dropdown();
    }

    function readTitles(subjectId) {
        fetch('${pageContext.request.contextPath}/title/read-all-by-subjectid/' + subjectId)
            .then(response => response.json())
            .then(data => {
                drawTitlesReadSubjects(data, subjectId);
            });
    }
</script>
</body>
</html>