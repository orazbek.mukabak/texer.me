<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-tasks.css"/>
</head>
<body>
<%@ include file="read-tasks-modal.jsp" %>
<%@ include file="read-tasks-modal-diagram.jsp" %>
<%@ include file="read-tasks-draw-mcq.jsp" %>
<script>
    function validateQuestionReadTasks(variantId, i) {
        let tasks = JSON.parse(localStorage.getItem("tasks_" + variantId));
        selectCurrentIndexReadTasks(variantId, tasks[i].index);
        disableButtonsReadTasks(tasks.length, tasks[i].index);
        let varHtml = drawQuestionMCQReadTasks(tasks[0].participant, variantId, tasks[i].question);
        document.getElementById("question_body_read_tasks").innerHTML = varHtml;
    }

    function drawTasksReadTasks(variantId) {
        let tasks = JSON.parse(localStorage.getItem("tasks_" + variantId));
        document.getElementById("question_bottom_read_tasks").innerHTML = drawQuestionBottomReadTasks(variantId, tasks);
        document.getElementById("modal_footer_read_tasks").innerHTML = drawModalFooterReadTasks(tasks[0].participant, variantId);
        if (localStorage.getItem("tasksCurrentIndex_" + variantId)) {
            let i = Number.parseInt(localStorage.getItem("tasksCurrentIndex_" + variantId)) - 1;
            validateQuestionReadTasks(variantId, i);
        } else {
            validateQuestionReadTasks(variantId, 0);
        }
    }

    function readQuestions(variantId, idList, tasks) {
        fetch('${pageContext.request.contextPath}/question/read-all-by-idlist/' + idList)
            .then(response => response.json())
            .then(data => {
                localStorage.setItem("tasks_" + variantId, JSON.stringify(mergeTaskQuestion(tasks, data)));
                localStorage.setItem("tasksLength_" + variantId, tasks.length);
                readParticipantReadTasks(variantId, "1");
            });
    }

    function readTasks(variantId, updatedDate) {
        if (!localStorage.getItem("variantUpdatedDate_" + variantId) || localStorage.getItem("variantUpdatedDate_" + variantId) !== updatedDate) {
            localStorage.setItem("variantUpdatedDate_" + variantId, updatedDate);
            fetch('${pageContext.request.contextPath}/task/read-all-by-variantid/' + variantId)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    let idList = createIdList(data);
                    readQuestions(variantId, idList, data);
                });
        } else {
            readParticipantReadTasks(variantId, "1");
        }

        modal.style.display = "block";
        document.getElementById("question_header_title_read_tasks").innerHTML = getEnvCurrentWebsiteUrl();
    }

    function mergeTaskQuestion(tasks, questions) {
        for (let i = 0; i < tasks.length; i++) {
            let index = i + 1;
            tasks[i].index = index;
            tasks[i].isAnswered = '0';
            tasks[i].isGenerated = false;
            for (let j = 0; j < questions.length; j++) {
                if (tasks[i].questionId === questions[j].id) {
                    tasks[i].question = questions[j];
                    tasks[i].question.index = index;
                    break;
                }
            }
        }
        return tasks;
    }

    function createIdList(data) {
        let idList = "";
        for (let i = 0; i < data.length; i++) {
            idList += data[i].questionId + ",";
        }
        return idList.slice(0, -1);
    }

    function readParticipantReadTasks(variantId, author) {
        fetch('${pageContext.request.contextPath}/participant/read-by-variantid-and-author/' + variantId + "?author=" + author)
            .then(response => response.json())
            .then(data => {
                alreadyCompletedReadTasks(data, variantId)
                drawTasksReadTasks(variantId);
            });
    }

    function alreadyCompletedReadTasks(participant, variantId) {
        let tasks = JSON.parse(localStorage.getItem("tasks_" + variantId));
        if(participant.status === 'f') {
            let arrayAnswers = participant.answers.split('OzK');
            let arrayCorrectAnswers = participant.correctAnswers.split('OzK');
            for (let i = 0; i < tasks.length; i++) {
                let index = i + 1;
                tasks[i].index = index;
                tasks[i].question.isAnswered = arrayAnswers[i];
                tasks[i].isAnswered = arrayAnswers[i];
                tasks[i].isGenerated = false;
                tasks[i].question.correctAnswer = arrayCorrectAnswers[i];
            }
        }
        tasks[0].participant = participant;
        localStorage.setItem("tasks_" + variantId, JSON.stringify(tasks));
    }
</script>
</body>
</html>