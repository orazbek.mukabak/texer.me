<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-question-mcq.css"/>
</head>
<body>
<script>
    function drawQuestionMCQ(question, index) {
        let varHtml = "<div class='question-mcq-wrapper-read-subjects'>";
        varHtml += "<div class='question-mcq-header-read-subjects'>";
        varHtml += "<div>№ " + index + "</div>";
        varHtml += "<div>ID: " + question.id + "</div>";
        varHtml += "<div>Дұрыс жауабы: " + question.correctAnswer + "</div>";
        varHtml += "</div>";
        varHtml += "<div class='question-mcq-body-read-subjects'>";
        varHtml += question.text;
        varHtml += "</div>";
        varHtml += "<div class='question-mcq-bottom-read-subjects'>";
        if (question.answerA.length > 0) {
            varHtml += "<div>A) " + question.answerA + "</div>";
        }
        if (question.answerB.length > 0) {
            varHtml += "<div>B) " + question.answerB + "</div>";
        }
        if (question.answerC.length > 0) {
            varHtml += "<div>C) " + question.answerC + "</div>";
        }
        if (question.answerD.length > 0) {
            varHtml += "<div>D) " + question.answerD + "</div>";
        }
        varHtml += "</div>";
        varHtml += "</div>";
        return varHtml;
    }
</script>
</body>
</html>