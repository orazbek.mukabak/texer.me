<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-tasks-modal-diagram.css"/>
</head>
<body>
<script>
    function showDiagramReadTasks(participant) {
        let percentage = Math.round(participant.totalCorrectQuestion * 100 / participant.totalQuestion);
        let varHtml = "<div class='analyze-header-read-tasks'>";

        varHtml += "<div class='analyze-header-conversation-read-tasks'>";
        varHtml += "<div class='analyze-header-diagram-read-tasks'>";
        varHtml += "<div class='pie no-round' style='--p:" + percentage + ";--c:#f7931e;--b:15px'>" + percentage + "%</div>";
        varHtml += "</div>";
        varHtml += "<div class='analyze-header-text-read-tasks'>Құрметті " + participant.lastName + " " + participant.firstName + ". Сіз берілген " + participant.totalQuestion + " тапсырманың " + participant.totalCorrectQuestion + " тапсырмасына дұрыс жауап бердіңіз. Үлгеріміңіз " + percentage + " пайыз.</div>";
        varHtml += "</div>";
        varHtml += "</div>";
        document.getElementById("question_header_read_tasks").innerHTML = "";
        document.getElementById("question_body_read_tasks").innerHTML = varHtml;
    }
</script>
</body>
</html>