<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/assessment/read-tasks-draw-mcq.css"/>
</head>
<body>
<script>
    function unselectClickedAnswerMCQReadTasks() {
        let answers = ["a", "b", "c", "d"]
        for (let i = 0; i < answers.length; i++) {
            if (document.getElementById("answer_" + answers[i] + "_wrapper_read_tasks")) {
                document.getElementById("answer_" + answers[i] + "_wrapper_read_tasks").style.border = "1px solid #ddd";
                document.getElementById("answer_" + answers[i] + "_wrapper_read_tasks").style.boxShadow = "none";
                document.getElementById("answer_" + answers[i] + "_head_read_tasks").style.color = "#ddd";
            }
        }
    }

    function selectClickedAnswerMCQReadTasks(answer, index) {
        unselectClickedAnswerMCQReadTasks();
        document.getElementById("answer_" + answer + "_wrapper_read_tasks").style.border = "1px solid #f7931e";
        document.getElementById("answer_" + answer + "_wrapper_read_tasks").style.boxShadow = "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px";
        document.getElementById("answer_" + answer + "_head_read_tasks").style.color = "#f7931e";
        document.getElementById("btn_question_bottom_read_tasks_" + index).style.border = "1px solid #f7931e";
        document.getElementById("btn_question_bottom_read_tasks_" + index).style.background = "#f7931e";
        document.getElementById("btn_question_bottom_read_tasks_" + index).style.color = "#fff";
        let button = document.getElementById("btn_question_bottom_read_tasks_" + index);
        button.style.borderBottom = "3px solid #222c31";
        button.style.fontWeight = "bold";
        button.style.color = "#222c31";
    }

    function clickAnswerMCQReadTasks(variantId, answer, index) {
        let tasks = JSON.parse(localStorage.getItem("tasks_" + variantId));
        let i = index - 1;
        tasks[i].isAnswered = answer;
        localStorage.setItem("tasks_" + variantId, JSON.stringify(tasks));
        selectClickedAnswerMCQReadTasks(answer, index);
    }

    function drawQuestionMCQReadTasks(participant, variantId, question) {
        drawQuestionHeaderReadTasks(variantId, question);
        let varHtml = "<div class='question-mcq-text-read-tasks'>";
        varHtml += question.text;
        varHtml += "</div>";
        varHtml += "<div class='question-mcq-answers-wrapper-read-tasks'>";
        if (question.answerA !== null) {
            if (question.answerA.length > 0) {
                varHtml += "<div class='question-mcq-answer-wrapper-read-tasks'>";
                varHtml += "<div class='question-mcq-answer-left-read-tasks'>A.</div>";
                if (participant.status === 'c') {
                    varHtml += "<div class='question-mcq-answer-right-read-tasks' id='answer_a_wrapper_read_tasks' onclick='clickAnswerMCQReadTasks(" + variantId + ", \"a\"," + question.index + ");'>";
                } else {
                    if (question.isAnswered === "a") {
                        if (question.correctAnswer === "a") {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks correct' id='answer_a_wrapper_read_tasks'>";
                        } else {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks wrong' id='answer_a_wrapper_read_tasks'>";
                        }
                    } else {
                        if (question.correctAnswer === "a") {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks correct' id='answer_a_wrapper_read_tasks'>";
                        } else {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks' id='answer_a_wrapper_read_tasks'>";
                        }
                    }
                }
                varHtml += "<div class='question-mcq-answer-head-read-tasks' id='answer_a_head_read_tasks'>&#10148;</div>";
                varHtml += "<div class='question-mcq-answer-text-read-tasks'>";
                varHtml += question.answerA;
                varHtml += "</div>";
                varHtml += "</div>";
                varHtml += "</div>";
            }
        }
        if (question.answerB !== null) {
            if (question.answerB.length > 0) {
                varHtml += "<div class='question-mcq-answer-wrapper-read-tasks'>";
                varHtml += "<div class='question-mcq-answer-left-read-tasks'>B.</div>";
                if (participant.status === 'c') {
                    varHtml += "<div class='question-mcq-answer-right-read-tasks' id='answer_b_wrapper_read_tasks' onclick='clickAnswerMCQReadTasks(" + variantId + ", \"b\"," + question.index + ");'>";
                } else {
                    if (question.isAnswered === "b") {
                        if (question.correctAnswer === "b") {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks correct' id='answer_b_wrapper_read_tasks'>";
                        } else {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks wrong' id='answer_b_wrapper_read_tasks'>";
                        }
                    } else {
                        if (question.correctAnswer === "b") {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks correct' id='answer_b_wrapper_read_tasks'>";
                        } else {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks' id='answer_b_wrapper_read_tasks'>";
                        }
                    }
                }
                varHtml += "<div class='question-mcq-answer-head-read-tasks' id='answer_b_head_read_tasks'>&#10148;</div>";
                varHtml += "<div class='question-mcq-answer-text-read-tasks'>";
                varHtml += question.answerB;
                varHtml += "</div>";
                varHtml += "</div>";
                varHtml += "</div>";
            }
        }
        if (question.answerC !== null) {
            if (question.answerC.length > 0) {
                varHtml += "<div class='question-mcq-answer-wrapper-read-tasks'>";
                varHtml += "<div class='question-mcq-answer-left-read-tasks'>C.</div>";
                if (participant.status === 'c') {
                    varHtml += "<div class='question-mcq-answer-right-read-tasks' id='answer_c_wrapper_read_tasks' onclick='clickAnswerMCQReadTasks(" + variantId + ", \"c\"," + question.index + ");'>";
                } else {
                    if (question.isAnswered === "c") {
                        if (question.correctAnswer === "c") {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks correct' id='answer_c_wrapper_read_tasks'>";
                        } else {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks wrong' id='answer_c_wrapper_read_tasks'>";
                        }
                    } else {
                        if (question.correctAnswer === "c") {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks correct' id='answer_c_wrapper_read_tasks'>";
                        } else {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks' id='answer_c_wrapper_read_tasks'>";
                        }
                    }
                }
                varHtml += "<div class='question-mcq-answer-head-read-tasks' id='answer_c_head_read_tasks'>&#10148;</div>";
                varHtml += "<div class='question-mcq-answer-text-read-tasks'>";
                varHtml += question.answerC;
                varHtml += "</div>";
                varHtml += "</div>";
                varHtml += "</div>";
            }
        }
        if (question.answerD !== null) {
            if (question.answerD.length > 0) {
                varHtml += "<div class='question-mcq-answer-wrapper-read-tasks'>";
                varHtml += "<div class='question-mcq-answer-left-read-tasks'>D.</div>";
                if (participant.status === 'c') {
                    varHtml += "<div class='question-mcq-answer-right-read-tasks' id='answer_d_wrapper_read_tasks' onclick='clickAnswerMCQReadTasks(" + variantId + ", \"d\"," + question.index + ");'>";
                } else {
                    if (question.isAnswered === "d") {
                        if (question.correctAnswer === "d") {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks correct' id='answer_d_wrapper_read_tasks'>";
                        } else {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks wrong' id='answer_d_wrapper_read_tasks'>";
                        }
                    } else {
                        if (question.correctAnswer === "d") {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks correct' id='answer_d_wrapper_read_tasks'>";
                        } else {
                            varHtml += "<div class='question-mcq-answer-right-read-tasks' id='answer_d_wrapper_read_tasks'>";
                        }
                    }
                }
                varHtml += "<div class='question-mcq-answer-head-read-tasks' id='answer_d_head_read_tasks'>&#10148;</div>";
                varHtml += "<div class='question-mcq-answer-text-read-tasks'>";
                varHtml += question.answerD;
                varHtml += "</div>";
                varHtml += "</div>";
                varHtml += "</div>";
            }
        }
        varHtml += "</div>";
        return varHtml;
    }
</script>
</body>
</html>