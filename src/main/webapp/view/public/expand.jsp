<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/expand.css" />
</head>
<body>
<div id="popupSetting" class="overlay-box">
    <div class="popup-inner-box">
        <div class="popup-header">
            Сайтты дамыту
            <a class="popup-close" href="#">&times;</a>
        </div>
        <div class="divider"></div>
        <div class="popup-body">
            <div class="collection">
                <a href="${pageContext.request.contextPath}/subject/edit" class="collection-item"><i class="material-icons-outlined">quiz</i>Сұрақ өңдеу</a>
                <a href="${pageContext.request.contextPath}/position/edit" class="collection-item"><i class="material-icons-outlined">work_outline</i>Жұмыс орындарын өңдеу</a>
                <a href="${pageContext.request.contextPath}/" class="collection-item"><i class="material-icons-outlined">tune</i>Қолданушыны өңдеу</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>