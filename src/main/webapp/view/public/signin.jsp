<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css" />
</head>
<body>
<div id="popupSignin" class="overlay-box">
    <div class="popup-inner-box">
        <div class="popup-header">
            <spring:message code="signin.signin" />
            <a class="popup-close" href="#">&times;</a>
        </div>
        <div class="divider"></div>
        <div class="popup-body">
            <div class="login-signin-wrapper-signin">
                <a href="${pageContext.request.contextPath}/?#popupSignup"><spring:message code="signin.need.account.message" /></a>
            </div>
            <form:form action="${pageContext.request.contextPath}/authenticate" method="POST" onsubmit="return validateSigninCredentials();">
                <div class="login-input-wrapper-signin">
                    <input id="signin_mobile" name="username" onClick="this.select();" placeholder='<spring:message code="signin.mobile.input" />' minlength="12" maxlength="12" />
                </div>
                <div class="login-input-wrapper-signin">
                    <input id="signin_password" type="password" name="password" placeholder='<spring:message code="signin.password.input" />' />
                </div>
                <button type="submit" class="btn login-btn-signin"><spring:message code="signin.signin.button" /></button>
            </form:form>
        </div>
        <div class="divider"></div>
        <div class="popup-footer">
            <a href="${pageContext.request.contextPath}/?#popupReset"><spring:message code="signin.forgot.message" /></a>
        </div>
    </div>
</div>
<script>
    function validateSigninCredentials() {
        let mobile = document.getElementById("signin_mobile").value;
        let password = document.getElementById("signin_password").value;
        if(mobile.length === 12 && password.length > 4) {
            return true;
        }
        M.toast({html: '<span><spring:message code="signin.error.message" /></span>',  classes: 'red accent-2'});
        return false;
    }
</script>
</body>
</html>