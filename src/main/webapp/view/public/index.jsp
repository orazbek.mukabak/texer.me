<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/index.css" />
    <title></title>
</head>
<body>
    <%@ include file="header.jsp" %>
    <%@ include file="../website/banner.jsp" %>
    <%@ include file="../employment/read-positions.jsp" %>
    <%@ include file="footer.jsp" %>
    <%@ include file="callback.jsp" %>
</body>
</html>