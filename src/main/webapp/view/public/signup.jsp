<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html><html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signup.css" />
</head>
<body>
<div id="popupSignup" class="overlay-box">
    <div class="popup-inner-box">
        <div class="popup-header">
            <spring:message code="signup.signup" />
            <a class="popup-close" href="#">&times;</a>
        </div>
        <div class="divider"></div>
        <div class="popup-body">
            <div class="login-signup-wrapper-signup">
                <a href="${pageContext.request.contextPath}/?#popupSignin"><spring:message code="signup.have.account.message" /></a>
            </div>
            <form>
                <div class="login-input-wrapper-signup">
                    <input id="mobile" onClick="this.select();" placeholder='<spring:message code="signup.mobile.input" />' minlength="12" maxlength="12" />
                </div>
                <div class="login-input-wrapper-signup">
                    <input id="firstName" placeholder='<spring:message code="signup.name.input" />' minlength="1" maxlength="31"/>
                </div>
                <div class="login-input-wrapper-signup">
                    <input id="lastName" placeholder='<spring:message code="signup.surname.input" />' minlength="1" maxlength="31"/>
                </div>
                <div class="login-input-wrapper-signup">
                    <input type="password" id="password" placeholder='<spring:message code="signup.password.input" />' />
                </div>
                <div class="login-input-wrapper-signup">
                    <input type="password" id="password2" placeholder='<spring:message code="signup.repeat.password.input" />' />
                </div>
                <button onclick="validateUserInfo();" class="btn login-btn-signup"><spring:message code="signup.signup.button" /></button>
            </form>
        </div>
        <div class="divider"></div>
        <div class="popup-footer">
            <a href="${pageContext.request.contextPath}/privacy-policy"><spring:message code="signup.accept.message" /></a>
        </div>
    </div>
</div>
<script>
    function validateUserInfo() {
        let mobile = document.getElementById("mobile").value;
        let firstName = document.getElementById("firstName").value;
        let lastName = document.getElementById("lastName").value;
        let password = document.getElementById("password").value;
        let password2 = document.getElementById("password2").value;

        if(mobile && firstName && lastName && password && password2) {
            if(password === password2) {
                if(password.length > 4 && password.length < 32) {
                    if(firstName.length < 32 && lastName.length < 32) {
                        if(mobile.length === 12) {
                            postUserInfo({"mobile": mobile, "password": password, "firstName": firstName, "lastName": lastName});
                        } else {
                            M.toast({html: '<span><spring:message code="signup.invalid.phone.message" /></span>',  classes: 'red accent-2'});
                        }
                    } else {
                        M.toast({html: '<span><spring:message code="signup.name.length.message" /></span>',  classes: 'red accent-2'});
                    }
                } else {
                    M.toast({html: '<span><spring:message code="signup.password.length.message" /></span>',  classes: 'red accent-2'});
                }
            } else {
                M.toast({html: '<span><spring:message code="signup.unmatched.message" /></span>',  classes: 'red accent-2'});
            }
        } else {
            M.toast({html: '<span><spring:message code="signup.fill-up.message" /></span>',  classes: 'red accent-2'});
        }
    }

    function postUserInfo(userInfo) {
        fetch('${pageContext.request.contextPath}/userinfo/create', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userInfo)
        })
            .then(response => response.json())
            .then(response => {
                if(response) {
                    if(response.id && response.id > 0) {
                        clearRegistrationForm();
                        M.toast({html: '<span><spring:message code="signup.successful.message" /></span>',  displayLength: 4000, classes: 'green', completeCallback:  () => { window.location.href = "?#popupSignin"; }});
                    } else if(response.errors[0] && response.errors[0].includes(userInfo.mobile)) {
                        M.toast({html: '<span>' + userInfo.mobile + ' <spring:message code="signup.already.registered.message" /></span>',  classes: 'red accent-2'});
                    } else {
                        M.toast({html: '<span><spring:message code="signup.tech.support.message" /></span>',  classes: 'red accent-2'});
                    }
                } else {
                    M.toast({html: '<span><spring:message code="signup.tech.support.message" /></span>',  classes: 'red accent-2'});
                }
            })
    }

    function clearRegistrationForm() {
        document.getElementById("mobile").value = ""
        document.getElementById("firstName").value = ""
        document.getElementById("lastName").value = ""
        document.getElementById("password").value = ""
        document.getElementById("password2").value = ""
    }
</script>
</body>
</html>